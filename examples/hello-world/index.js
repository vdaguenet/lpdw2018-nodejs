// console.log('Hello world');

// include HTTP module
const http = require('http');

// Define host and port
const host = '127.0.0.1';
const port = 3000;

// Handle request
const server = http.createServer((req, res) => {
  // Send response
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.end('Hello world');
});

// Let's go!
server.listen(port, host, () => {
  console.log(`Server running at http://${host}:${port}/`);
});
