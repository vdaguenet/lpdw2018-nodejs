// Include Express
const express = require('express');
// Create application
const app = express();

// Define host and port
const host = '127.0.0.1';
const port = 3000;

// Handle request on route /
app.get('/', (req, res) => {
  // Send response
  res.send('Hello World!');
});

// Let's go!
app.listen(port, host, () => {
  console.log(`Server running at http://${host}:${port}/`);
});
