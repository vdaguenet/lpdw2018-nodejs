module.exports = {
  server: {
    host: 'localhost',
    port: 3000
  },
  database: {
    url: 'mongodb://localhost:27017',
    name: 'lpdw2018'
  }
};
