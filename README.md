# LPDW2018 - Node.js

## Installation du projet
Dans un terminal, lancez cette commande pour installer les modules nécessaires
```
npm install
```

Pour installer la base de données:
1. Lancez MongoDB
```
mongod --dbpath=./data
```
2. Insérez les données (dans un autre terminal)
```
node db-creator.js
```
## Démarrer le projet
Une fois l'installation faite, démarrez Mongo puis le serveur (dans un autre terminal) avec
```
npm start
```

Vous pouvez voir le résultat sur [localhost:3000](localhost:3000)


## Différences notables avec le cours
1. Dans le fichier `index.js`, vous pouvez voir que les appels à la base de données sont fait avec la classe `Database` afin de réduire la répétition des lignes avec `MongoClient.connect`, `db.collection`, etc...
2. Vous pouvez voir l'utilisation de string de ce genre `http://${config.server.host}:${config.server.port}/`. Ce sont des template strings diponibles en ES6, elles permettent de concatener une string et des variables. En JavaScript « classique » nous aurions écrit: `'http://' + config.server.host + ':' + config.server.port + '/'`.


## Les fichiers et dossiers

### index.js
C'est le point d'entrée du programme, le « main ». Nous y trouvons la configuration du serveur, de l'application Express ainsi que les différentes fonctions pour la navigation.

## config.js
Fichier contenant les variables de configuration pour le serveur et la base de données.

## Database.js
C'est une classe qui fait interface avec le MongoClient. Ceci permet de réduire la taille du fichier index.js et ainsi faciliter sa compréhension. Dans ce fichier vous allez trouver beaucoup de JavaScript ES6: Class, Arrow Functions, Promises, ... Je ne vais pas expliquer ces éléments ici, nous les aborderons ensemble lors de notre prochaine rencontre.

## /templates
Contient les templates Handlebar que nous avons écrit ensemble.

## /examples
Vous y trouverez les premiers programmes que nous avons fait: Hello World avec le module `HTTP` et le Hello World avec `Express`.
