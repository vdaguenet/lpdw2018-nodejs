// Include necessary modules
const express = require('express');
const path = require('path');
const hbs = require('express-hbs');
const config = require('./config.js'); // include our config file
const Database = require('./Database.js'); // include our database utility

/* ==========================================*
 * Express application settings
 * ============================================ */

// Create app
const app = express();

// Use middlewares to parse request body
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Register template engine
app.engine('hbs', hbs.express4({
  layoutsDir: path.join(__dirname, 'templates/layouts')
}));
// Use the desired template engine
app.set('view engine', 'hbs');
// Set path to templates
app.set('views', path.join(__dirname, 'templates'));



/* ==========================================*
 * ROUTING (pages)
 * ============================================ */

// Home page
// catch request on localhost:3000
app.get('/', function(req, res) {
  Database.findAllUsers()
    .then((users) => {
      // Send HTML response of the template index compiled
      // with the given object of variables
      res.render('index', {
        users: users
      });
    })
    .catch((err) => {
      // Catch errors
      console.log('Oups!', err)
    });
});

// User page
// catch request on localhost:3000/user/alan-turing for example
app.get('/user/:slug', function(req, res) {
  Database.findUser(req.params.slug) // URL parameters (slug) are in req.params object
    .then((user) => {
      // Send HTML response of the template profile compiled
      // with the user infos
      res.render('profile', {
        firstname: user.firstname,
        lastname: user.lastname,
        age: user.age
      });
    })
    .catch((err) => {
      // Catch errors
      console.log('Oups!', err)
    });
});



/* ==========================================*
 * API
 * ============================================ */

// get all users
// catch request to localhost:3000/api/users with method GET
app.get('/api/users', function(req, res) {
  Database.findAllUsers()
    .then((users) => {
      // Send array of users as JSON
      res.json(users);
    })
    .catch((err) => {
      console.log('Oups!', err)
    });
});

// create user
// catch request to localhost:3000/api/users with method PUT
app.put('/api/users', function(req, res) {
  Database.addUser(req.body) // user infos are in request body
    .then((user) => {
      // Send creation confirmation
      // with the created user
      res.json({
        status: 'created',
        user // equivalent to user: user
      });
    })
    .catch((err) => {
      console.log('Oups!', err)
    });
});

// get user
// catch request to localhost:3000/api/users/alan-turing (for example) with method GET
app.get('/api/users/:slug', function(req, res) {
  Database.findUser(req.params.slug)
    .then((user) => {
      res.json(user);
    })
    .catch((err) => {
      console.log('Oups!', err)
    });
});

// update user
// catch request to localhost:3000/api/users/alan-turing (for example) with method POST
app.post('/api/users/:slug', function(req, res) {
  Database.updateUser(req.params.slug, req.body)
    .then(() => {
      // Send update confirmation
      res.json({
        status: 'updated'
      });
    })
    .catch((err) => {
      console.log('Oups!', err)
    });
});

// delete user
// catch request to localhost:3000/api/users/alan-turing (for example) with method DELETE
app.delete('/api/users/:slug', function(req, res) {
  Database.deleteUser(req.params.slug)
    .then(() => {
      // Send deletion confirmation
      res.json({
        status: 'deleted'
      });
    })
    .catch((err) => {
      console.log('Oups!', err)
    });
});



/* ==========================================*
 * Start server
 * ============================================ */

app.listen(config.server.port, config.server.host, () => {
  console.log(`Server running at http://${config.server.host}:${config.server.port}/`);
});


