// Include Mongo driver
const MongoClient = require('mongodb').MongoClient;
// Include config files
const config = require('./config.js');

/**
 * class: Database
 * Manage Mongo server connection and every opertions (search, create, ...)
 */
class Database {
  constructor() {
    // Reference to the database instance
    this.db = undefined;
  }

  /**
   * Connect to Mongo server
   * @return {Promise}
   */
  connect() {
    return new Promise((resolve, reject) => {
      // Connect to Mongo server
      MongoClient.connect(config.database.url, { useNewUrlParser: true }, (err, client) => {
        if (err) {
          // Reject Promise in case of error
          reject(err);
          return;
        }

        // Save database reference
        this.db = client.db(config.database.name);
        console.log('[Database] Connected to database');

        // Resolve Promise to go to the next operation
        resolve();
      });
    })
  }

  /**
   * Get all users in database
   * @return {Promise}
   */
  findAllUsers() {
    if (!this.db) {
      // If we don't have a database reference
      // we connect to the server
      return this.connect()
        .then(this.findAllUsers.bind(this)); // Once connected, relaunch findAllUsers
    }

    return new Promise((resolve, reject) => {
      // Choose collection users
      this.db.collection('users')
        // call find() with no filters, so it will retriev every users
        .find({})
        // convert result to array
        .toArray((err, users) => {
          if (err) {
            // Reject Promise in case of error
            reject(err);
            return;
          }

          // send array of users to the next operation
          resolve(users);
        });
    })
  }

  /**
   * Find desired user
   * @param  {String} slug
   * @return {Promise}
   */
  findUser(slug) {
    if (!this.db) {
      // If we don't have a database reference
      // we connect to the server
      return this.connect()
        .then(this.findUser.bind(this, slug)); // Once connected, relaunch findUser
    }

    return new Promise((resolve, reject) => {
      // Choose collection users
      this.db.collection('users')
        // find users and filter result by slug
        .find({slug})
        // convert result to array
        .toArray((err, users) => {
          if (err) {
            // Reject Promise in case of error
            reject(err);
            return;
          }

          if (users[0]) {
            // send found users to the next operation
            resolve(users[0]);
          } else {
            // if no result, send default values
            resolve({
              firstname: 'N/A',
              lastname: 'N/A',
              age: 0
            });
          }
        });
    })
  }

  /**
   * create new user
   * @param {Array} data
   */
  addUser(data) {
    if (!this.db) {
      // If we don't have a database reference
      // we connect to the server
      return this.connect()
        .then(this.addUser.bind(this, data)); // Once connected, relaunch addUser
    }

    // Create slug
    const fullname = `${data.firstname} ${data.lastname}`;
    data.slug = fullname.toLowerCase()
      .trim()
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(/[^\w-]+/g, '') // Remove all non-word chars
      .replace(/--+/g, '-'); // Replace multiple - with single -

    return new Promise((resolve, reject) => {
      // Choose collection users
      this.db.collection('users')
        // create new user with infos from data
        .insertOne(data, (err, res) => {
          if (err) {
            // Reject Promise in case of error
            reject(err);
            return;
          }

          if (res.result.n === 1) {
            console.log(`[Database] Created user`);
            // send created user to the next operation
            resolve(res.ops[0]);
          } else {
            // reject with an error if user isn't created
            reject(new Error('An error occured while creating user'));
          }
        });
    });
  }

  updateUser(slug, data) {
    if (!this.db) {
      // If we don't have a database reference
      // we connect to the server
      return this.connect()
        .then(this.updateUser.bind(this, slug, data)); // Once connected, relaunch updateUser
    }

    return new Promise((resolve, reject) => {
      // Choose collection users
      this.db.collection('users')
        .updateOne(
          { slug },
          { $set: data },
          (err, res) => {
            if (err) {
              // Reject Promise in case of error
              reject(err);
              return;
            }

            if (res.result.n === 1) {
              console.log(`[Database] Updated ${slug}`);
              resolve();
            } else {
              reject(new Error('An error occured while updating user'));
            }
          });
    });
  }

  deleteUser(slug) {
    if (!this.db) {
      // If we don't have a database reference
      // we connect to the server
      return this.connect()
        .then(this.deleteUser.bind(this, slug)); // Once connected, relaunch deleteUser
    }

    return new Promise((resolve, reject) => {
      // Choose collection users
      this.db.collection('users')
        .deleteOne(
          { slug },
          (err, res) => {
            if (err) {
              // Reject Promise in case of error
              reject(err);
              return;
            }

            if (res.result.n === 1) {
              console.log(`[Database] Deleted ${slug}`);
              resolve();
            } else {
              reject(new Error('An error occured while deleting user'));
            }
          });
    });
  }
}

// Export a Database object to have a kind of singleton
module.exports = new Database();