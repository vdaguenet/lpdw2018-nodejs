// Include Mongo driver
const MongoClient = require('mongodb').MongoClient;
// Include our config file
const config = require('./config.js')

// Connect to Mongo server
MongoClient.connect(config.database.url, { useNewUrlParser: true }, (err, client) => {
  if (err) {
    console.error(err);
    return;
  }

  console.log('Connected to database');

  // Choose database
  const db = client.db(config.database.name);

  // Create users collection
  db.createCollection('users')
    .then((result) => {
      console.log('Collection created')
    })
    .then(() => {
      // When collection is created, insert elements
      return db.collection('users').insertMany([
        { firstname: 'Alan', lastname: 'Turing', slug: 'alan-turing', age: 41 },
        { firstname: 'Linus', lastname: 'Torvalds', slug: 'linus-torvalds', age: 48 },
        { firstname: 'Richard', lastname: 'Stallman', slug: 'richard-stallman', age: 65 },
        { firstname: 'Tim', lastname: 'Berners Lee', slug: 'tim-berners-lee', age: 63 }
      ])
    })
    .then((res) => {
      console.log('Insertion done', res.result)
      // Close connection when insertion is done
      client.close();
    })
    .catch((err) => {
      // Catch errors
      console.log('ERROR:', err)
      client.close();
    })
});